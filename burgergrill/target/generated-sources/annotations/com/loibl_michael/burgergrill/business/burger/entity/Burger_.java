package com.loibl_michael.burgergrill.business.burger.entity;

import com.loibl_michael.burgergrill.business.burger.entity.Ingredient;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.3.v20180807-rNA", date="2019-01-20T18:25:00")
@StaticMetamodel(Burger.class)
public class Burger_ { 

    public static volatile SingularAttribute<Burger, String> burgerName;
    public static volatile SetAttribute<Burger, Ingredient> ingredients;
    public static volatile SingularAttribute<Burger, Long> id;

}