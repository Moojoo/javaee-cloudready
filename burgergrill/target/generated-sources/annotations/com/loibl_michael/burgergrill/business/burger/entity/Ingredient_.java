package com.loibl_michael.burgergrill.business.burger.entity;

import com.loibl_michael.burgergrill.business.burger.entity.Burger;
import com.loibl_michael.burgergrill.business.burger.entity.IngredientName;
import com.loibl_michael.burgergrill.business.burger.entity.IngredientType;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.3.v20180807-rNA", date="2019-01-20T18:25:00")
@StaticMetamodel(Ingredient.class)
public class Ingredient_ { 

    public static volatile SingularAttribute<Ingredient, IngredientType> ingredientType;
    public static volatile SingularAttribute<Ingredient, Integer> stockAmount;
    public static volatile SingularAttribute<Ingredient, IngredientName> ingredientName;
    public static volatile SingularAttribute<Ingredient, Integer> calorie;
    public static volatile SingularAttribute<Ingredient, Long> id;
    public static volatile SetAttribute<Ingredient, Burger> burgers;

}