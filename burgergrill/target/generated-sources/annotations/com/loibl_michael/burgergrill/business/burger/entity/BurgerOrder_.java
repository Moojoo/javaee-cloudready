package com.loibl_michael.burgergrill.business.burger.entity;

import com.loibl_michael.burgergrill.business.burger.entity.Burger;
import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.3.v20180807-rNA", date="2019-01-20T18:25:00")
@StaticMetamodel(BurgerOrder.class)
public class BurgerOrder_ { 

    public static volatile SingularAttribute<BurgerOrder, LocalDateTime> createdAt;
    public static volatile SingularAttribute<BurgerOrder, String> orderNumber;
    public static volatile SingularAttribute<BurgerOrder, Long> preparationTimeInSec;
    public static volatile SingularAttribute<BurgerOrder, Burger> burger;
    public static volatile SingularAttribute<BurgerOrder, Double> totalPrice;
    public static volatile SingularAttribute<BurgerOrder, Integer> calorie;
    public static volatile SingularAttribute<BurgerOrder, Long> id;
    public static volatile SingularAttribute<BurgerOrder, String> customerName;

}