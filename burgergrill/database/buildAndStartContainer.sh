#!/bin/bash

IMAGE_NAME=burgergrill-postgres-image
CONTAINER_NAME=burgergrill-db

docker build -f Dockerfile -t $IMAGE_NAME ./ &&
docker run -p 5432:5432 --name $CONTAINER_NAME -d $IMAGE_NAME