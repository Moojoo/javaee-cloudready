------------------------------------------------------------------------------------------------------------------
-- Author: Loibl Michael
-- Date:   28.12.2018
-- Reason: sql-file that initialize the burgergrill-db tables and fill the ingredients table after creation with all available ingredients.
------------------------------------------------------------------------------------------------------------------

--create tables

CREATE TABLE burgerorder(
    ID                          SERIAL          NOT NULL,
    CUSTOMER_NAME               VARCHAR(50)     NOT NULL,
    ORDER_NUMBER                VARCHAR(50)     NOT NULL,
    TOTAL_PRICE                 NUMERIC(30, 20) NOT NULL,
    CALORIE                     INTEGER         NULL,
    PREPARATION_TIME_IN_SEC     INTEGER         NOT NULL,
    CREATED_AT                  TIMESTAMP       NULL,
    ID_BURGER                   INTEGER         NOT NULL,
	PRIMARY KEY(ID));

CREATE TABLE burger(
    ID              SERIAL      NOT NULL,
    BURGER_NAME     VARCHAR(50) NOT NULL,
	PRIMARY KEY(ID));

CREATE TABLE ingredient(
    ID              SERIAL      NOT NULL,
    INGREDIENT_TYPE VARCHAR(50) NOT NULL,
    INGREDIENT_NAME VARCHAR(50) NOT NULL,
    STOCK_AMOUNT    INTEGER     NOT NULL,
    CALORIE         INTEGER     NOT NULL,
	PRIMARY KEY(ID));

CREATE TABLE burger_ingredient(
    ID_BURGER       INTEGER REFERENCES burger(ID),
    ID_INGREDIENT   INTEGER REFERENCES ingredient(ID),
    PRIMARY KEY(ID_BURGER, ID_INGREDIENT));

ALTER TABLE burgerorder ADD CONSTRAINT FK_BURGER FOREIGN KEY (ID_BURGER) REFERENCES burger(ID);

--insert ingredients

INSERT INTO ingredient (INGREDIENT_TYPE, INGREDIENT_NAME, STOCK_AMOUNT, CALORIE)
VALUES('BUN', 'FULL_GRAIN', 250, 280);

---

INSERT INTO ingredient (INGREDIENT_TYPE, INGREDIENT_NAME, STOCK_AMOUNT, CALORIE)
VALUES('PATTY', 'BEEF', 250, 250);

INSERT INTO ingredient (INGREDIENT_TYPE, INGREDIENT_NAME, STOCK_AMOUNT, CALORIE)
VALUES('PATTY', 'VEGETABLE_PATTY', 150, 60);

---

INSERT INTO ingredient (INGREDIENT_TYPE, INGREDIENT_NAME, STOCK_AMOUNT, CALORIE)
VALUES('CHEESE', 'CHEDDAR', 250, 402);

---

INSERT INTO ingredient (INGREDIENT_TYPE, INGREDIENT_NAME, STOCK_AMOUNT, CALORIE)
VALUES('VEGETABLES', 'TOMATO', 250, 10);

INSERT INTO ingredient (INGREDIENT_TYPE, INGREDIENT_NAME, STOCK_AMOUNT, CALORIE)
VALUES('VEGETABLES', 'CUCUMBER', 250, 12);

INSERT INTO ingredient (INGREDIENT_TYPE, INGREDIENT_NAME, STOCK_AMOUNT, CALORIE)
VALUES('VEGETABLES', 'SALAT', 250, 5);

---

INSERT INTO ingredient (INGREDIENT_TYPE, INGREDIENT_NAME, STOCK_AMOUNT, CALORIE)
VALUES('SAUCE', 'KETCHUP', 250, 112);

INSERT INTO ingredient (INGREDIENT_TYPE, INGREDIENT_NAME, STOCK_AMOUNT, CALORIE)
VALUES('SAUCE', 'AMERICAN_BURGER_SAUCE', 250, 140);

INSERT INTO ingredient (INGREDIENT_TYPE, INGREDIENT_NAME, STOCK_AMOUNT, CALORIE)
VALUES('SAUCE', 'VEGETABLE_SOUCE', 250, 10);

