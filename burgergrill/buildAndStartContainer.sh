#!/bin/bash
mvn clean install &&
docker build -f Dockerfile -t burgergrillimage ./ &&
docker run -d -t -p 9800:8080 -p 9009:9009 -p 4848:4848 --name burgergrill burgergrillimage
