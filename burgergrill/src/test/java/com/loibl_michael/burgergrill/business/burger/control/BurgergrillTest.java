package com.loibl_michael.burgergrill.business.burger.control;

import com.loibl_michael.burgergrill.business.burger.control.repository.RepositoryFactory;
import com.loibl_michael.burgergrill.business.burger.control.validation.BurgerNameValidator;
import com.loibl_michael.burgergrill.business.setting.boundary.SettingService;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.hamcrest.CoreMatchers.is;

/**
 *  @author Loibl_Michael
 *  @since 18.01.2019
 *  
 */
public class BurgergrillTest {

    @InjectMocks
    private Burgergrill burgergrill;

    @Mock
    private BurgerNameValidator burgerNameValidator;

    @Mock
    private SettingService settingService;

    @Mock
    private RepositoryFactory repositoryFactory;

    @Before
    public void initTest() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void successfulTest(){
        boolean testTrue = false;
        assertThat(testTrue, is(true));
    }


    @Test
    public void createBurgerForBurgerNameShouldRunSuccessful() {
        String burgerName = "testBurger";

        //given
        doNothing().when(burgerNameValidator).validate(burgerName);


        //when
        // TODO: 20.01.2019

        //then
        // TODO: 20.01.2019
        assertThat(true, is(true));
    }
}
