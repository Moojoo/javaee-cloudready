package com.loibl_michael.burgergrill.business.setting.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.loibl_michael.burgergrill.business.burger.entity.IngredientName;
import com.loibl_michael.burgergrill.business.burger.entity.IngredientType;

/**
 *  @author Loibl_Michael
 *  @since 31.12.2018
 *  
 */
public class ReceipeEntry {

    @JsonIgnore
    private IngredientType ingredientType;

    private IngredientName ingredientName;

    @JsonIgnore
    private int amount;

    public ReceipeEntry() {
    }

    public ReceipeEntry(IngredientType ingredientType, IngredientName ingredientName, int amount) {
        this.ingredientType = ingredientType;
        this.ingredientName = ingredientName;
        this.amount = amount;
    }

    public IngredientType getIngredientType() {
        return ingredientType;
    }

    public void setIngredientType(IngredientType ingredientType) {
        this.ingredientType = ingredientType;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public IngredientName getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(IngredientName ingredientName) {
        this.ingredientName = ingredientName;
    }
}
