package com.loibl_michael.burgergrill.business.burger.control.repository.order;

import com.loibl_michael.burgergrill.business.burger.control.repository.RepoQualifier;
import com.loibl_michael.burgergrill.business.burger.entity.BurgerOrder;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

import static com.loibl_michael.burgergrill.business.burger.control.repository.RepositoryType.ORDER_REPOSITORY;

/**
 *  @author Loibl_Michael
 *  @since 31.12.2018
 *  
 */
@RepoQualifier(forRepo = ORDER_REPOSITORY)
public class OrderRepository implements IOrderRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private Logger logger;

    @Override
    public void persist(BurgerOrder obj) {
        entityManager.persist(obj);
        logger.info("new order with orderNumber '{}' successful persisted", obj.getOrderNumber());
    }

    @Override
    public List<BurgerOrder> getAll() {
        List<BurgerOrder> allOrders = entityManager
                .createNamedQuery(BurgerOrder.FIND_ALL_ORDERS).getResultList();
        return allOrders;
    }

    @Override
    public BurgerOrder getOrderByOrderNumber(String orderNumber) {
        Query queryFindOrderByOrderNumber = entityManager.createNamedQuery(BurgerOrder.FIND_ORDER_BY_ORDER_NUMBER);
        queryFindOrderByOrderNumber.setParameter(BurgerOrder.ORDER_NUMBER_FOR_QUERY, orderNumber);
        BurgerOrder burgerOrder = (BurgerOrder) queryFindOrderByOrderNumber.getSingleResult();
        return burgerOrder;
    }
}
