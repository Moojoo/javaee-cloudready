package com.loibl_michael.burgergrill.business.setting.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *  @author Loibl_Michael
 *  @since 31.12.2018
 *  
 */
public class BurgerSetting {

    private List<Receipe> receipes;

    public BurgerSetting(){
        this.receipes = new ArrayList<>();
    }

    public List<Receipe> getReceipes() {
        return receipes;
    }

    public void setReceipes(List<Receipe> receipes) {
        this.receipes = receipes;
    }
}
