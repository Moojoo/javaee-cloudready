package com.loibl_michael.burgergrill.business.burger.control.validation;

/**
 *  @author Loibl_Michael
 *  @since 05.01.2019
 *  
 */
public class WrongBurgerNameException extends RuntimeException {

    public WrongBurgerNameException(String message) {
        super(message);
    }
}
