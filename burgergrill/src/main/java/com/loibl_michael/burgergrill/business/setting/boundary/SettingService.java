package com.loibl_michael.burgergrill.business.setting.boundary;

import com.loibl_michael.burgergrill.business.setting.entity.BurgerSetting;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Provider;

/**
 *  @author Loibl_Michael
 *  @since 27.12.2018
 *  
 */
@ApplicationScoped
public class SettingService {

    @Inject
    @ConfigProperty(name = "receipes")
    private Provider<BurgerSetting> burgerSettingProvider;

    public BurgerSetting getBurgerSetting() {
        return burgerSettingProvider.get();
    }
}
