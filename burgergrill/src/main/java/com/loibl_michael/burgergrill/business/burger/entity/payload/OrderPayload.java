package com.loibl_michael.burgergrill.business.burger.entity.payload;

/**
 *  @author Loibl_Michael
 *  @since 23.12.2018
 *  
 */
public class OrderPayload {

    private String customerName;
    private String burgerName;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getBurgerName() {
        return burgerName;
    }

    public void setBurgerName(String burgerName) {
        this.burgerName = burgerName;
    }

    @Override
    public String toString() {
        return "OrderPayload { " +
                "customerName = '" + customerName + '\'' +
                ", burgerName = '" + burgerName + '\'' +
                '}';
    }
}
