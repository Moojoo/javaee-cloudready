package com.loibl_michael.burgergrill.business.burger.entity;

/**
 *  @author Loibl_Michael
 *  @since 23.12.2018
 *  
 */
public enum IngredientType {
    BUN, PATTY, CHEESE, VEGETABLES, SAUCE
}
