package com.loibl_michael.burgergrill.business.burger.control.repository.burger;

import com.loibl_michael.burgergrill.business.burger.control.repository.IRepository;
import com.loibl_michael.burgergrill.business.burger.entity.Burger;

/**
 *  @author Loibl_Michael
 *  @since 04.01.2019
 *  
 */
public interface IBurgerRepository extends IRepository<Burger> {

    //currently, no specific burger-queries are necessary...
}
