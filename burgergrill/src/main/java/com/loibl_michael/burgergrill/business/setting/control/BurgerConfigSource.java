package com.loibl_michael.burgergrill.business.setting.control;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.microprofile.config.spi.ConfigSource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 *  @author Loibl_Michael
 *  @since 27.12.2018
 *  
 */
public class BurgerConfigSource implements ConfigSource {

    private static final String CONFIG_FILE_PATH = "/config/burgerSetting.json";

    @Override
    public Map<String, String> getProperties() {
        try {
            String jsonConfigAsString = new String(Files.readAllBytes(Paths.get(CONFIG_FILE_PATH)), StandardCharsets.UTF_8);
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, String> jsonConfigAsMap = objectMapper.readValue(jsonConfigAsString,
                    new TypeReference<Map<String, String>>() {
                    });
            return jsonConfigAsMap;
        } catch (IOException ex) {
            throw new RuntimeException("ERROR, could not initialize config-file: " + CONFIG_FILE_PATH, ex);
        }
    }

    @Override
    public Set<String> getPropertyNames() {
        return getProperties().keySet();
    }

    @Override
    public int getOrdinal() {
        return 900;
    }

    @Override
    public String getValue(String s) {
        return getProperties().get(s);
    }

    @Override
    public String getName() {
        return "config-file: " + CONFIG_FILE_PATH;
    }
}
