package com.loibl_michael.burgergrill.business.burger.control.repository.ingredient;

import com.loibl_michael.burgergrill.business.burger.control.repository.RepoQualifier;
import com.loibl_michael.burgergrill.business.burger.entity.Ingredient;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static com.loibl_michael.burgergrill.business.burger.control.repository.RepositoryType.INGREDIENT_REPOSITORY;

/**
 *  @author Loibl_Michael
 *  @since 01.01.2019
 *  
 */
@RepoQualifier(forRepo = INGREDIENT_REPOSITORY)
public class IngredientRepository implements IIngredientRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private Logger logger;

    @Override
    public void persist(Ingredient obj) {
        entityManager.persist(obj);
    }

    @Override
    public List<Ingredient> getAll() {
        List<Ingredient> ingredients = entityManager.createNamedQuery(Ingredient.FIND_ALL_INGREDIENTS).getResultList();
        return ingredients;
    }
}
