package com.loibl_michael.burgergrill.business.burger.control.repository.order;

import com.loibl_michael.burgergrill.business.burger.control.repository.IRepository;
import com.loibl_michael.burgergrill.business.burger.entity.BurgerOrder;

/**
 *  @author Loibl_Michael
 *  @since 04.01.2019
 *  
 */
public interface IOrderRepository extends IRepository<BurgerOrder> {

    BurgerOrder getOrderByOrderNumber(String orderNumber);
}
