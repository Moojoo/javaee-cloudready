package com.loibl_michael.burgergrill.business.burger.control.validation;

import com.loibl_michael.burgergrill.business.setting.boundary.SettingService;

import javax.inject.Inject;
import javax.validation.ValidationException;

/**
 *  @author Loibl_Michael
 *  @since 31.12.2018
 *  
 */
public class BurgerNameValidator {

    @Inject
    private SettingService settingService;

    public void validate(String burgerName) {
        settingService.getBurgerSetting().getReceipes().stream()
                .filter(x -> x.getBurgerName().equals(burgerName))
                .findFirst()
                .orElseThrow(() -> new WrongBurgerNameException("validation error, burgerName '" + burgerName + "' is not valid!"));
    }
}
