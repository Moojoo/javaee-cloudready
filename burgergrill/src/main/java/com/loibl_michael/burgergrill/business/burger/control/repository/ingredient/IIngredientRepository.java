package com.loibl_michael.burgergrill.business.burger.control.repository.ingredient;

import com.loibl_michael.burgergrill.business.burger.control.repository.IRepository;
import com.loibl_michael.burgergrill.business.burger.entity.Ingredient;

/**
 *  @author Loibl_Michael
 *  @since 04.01.2019
 *  
 */
public interface IIngredientRepository extends IRepository<Ingredient> {

    //currently, no specific ingredient-queries are necessary...
}
