package com.loibl_michael.burgergrill.business.burger.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 *  @author Loibl_Michael
 *  @since 23.12.2018
 *  
 */
@Entity
@Table(name = "ingredient")
@NamedQueries({
        @NamedQuery(
                name = Ingredient.FIND_ALL_INGREDIENTS, query = "SELECT ingr FROM Ingredient AS ingr"
        )
})
public class Ingredient implements Serializable {

    public static final String FIND_ALL_INGREDIENTS = "Ingredient.FIND_ALL_INGREDIENTS";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "INGREDIENT_NAME")
    @Enumerated(EnumType.STRING)
    private IngredientName ingredientName;

    @Column(name = "INGREDIENT_TYPE")
    @Enumerated(EnumType.STRING)
    private IngredientType ingredientType;

    @Column(name = "STOCK_AMOUNT")
    private int stockAmount;

    @Column(name = "CALORIE")
    private int calorie;

    @ManyToMany(mappedBy = "ingredients")
    private Set<Burger> burgers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IngredientType getIngredientType() {
        return ingredientType;
    }

    public void setIngredientType(IngredientType ingredientType) {
        this.ingredientType = ingredientType;
    }

    public int getStockAmount() {
        return stockAmount;
    }

    public void setStockAmount(int stockAmount) {
        this.stockAmount = stockAmount;
    }

    public int getCalorie() {
        return calorie;
    }

    public void setCalorie(int calorie) {
        this.calorie = calorie;
    }

    public Set<Burger> getBurgers() {
        return burgers;
    }

    public void setBurgers(Set<Burger> burgers) {
        this.burgers = burgers;
    }

    public IngredientName getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(IngredientName ingredientName) {
        this.ingredientName = ingredientName;
    }
}
