package com.loibl_michael.burgergrill.business.burger.control.repository;

import java.util.List;

/**
 *  @author Loibl_Michael
 *  @since 04.01.2019
 *  
 */
public interface IRepository<TYPE> {

    void persist(TYPE obj);
    List<TYPE> getAll();

    //currently, not all CRUD-operations are necessary
}
