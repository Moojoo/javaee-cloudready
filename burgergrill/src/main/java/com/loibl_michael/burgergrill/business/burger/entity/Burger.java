package com.loibl_michael.burgergrill.business.burger.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *  @author Loibl_Michael
 *  @since 28.12.2018
 *  
 */
@Entity
@Table(name = "burger")
@NamedQueries({
        @NamedQuery(
                name = Burger.FIND_ALL_BURGERS, query = "SELECT bur FROM Burger AS bur"
        )
})
public class Burger implements Serializable {

    public static final String FIND_ALL_BURGERS = "Burger.FIND_ALL_BURGERS";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @JsonIgnore
    private Long id;

    @Column(name = "BURGER_NAME")
    private String burgerName;

    @ManyToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "burger_ingredient",
            joinColumns = @JoinColumn(name = "ID_BURGER"),
            inverseJoinColumns = @JoinColumn(name = "ID_INGREDIENT")
    )
    @JsonIgnore
    private Set<Ingredient> ingredients;

    public Burger(){
        this.ingredients = new HashSet<>();
    }

    public static Burger createBurgerWithoutIngredients(String burgerName){
        Burger burger = new Burger();
        burger.setBurgerName(burgerName);
        return burger;
    }

    public void addIngredient(Ingredient ingredient){
        ingredients.add(ingredient);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBurgerName() {
        return burgerName;
    }

    public void setBurgerName(String burgerName) {
        this.burgerName = burgerName;
    }

    public Set<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
