package com.loibl_michael.burgergrill.business.burger.control.repository.burger;

import com.loibl_michael.burgergrill.business.burger.control.repository.RepoQualifier;
import com.loibl_michael.burgergrill.business.burger.entity.Burger;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static com.loibl_michael.burgergrill.business.burger.control.repository.RepositoryType.BURGER_REPOSITORY;

/**
 *  @author Loibl_Michael
 *  @since 31.12.2018
 *  
 */
@RepoQualifier(forRepo = BURGER_REPOSITORY)
public class BurgerRepository implements IBurgerRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private Logger logger;

    @Override
    public void persist(Burger obj) {
        entityManager.persist(obj);
        logger.info("new burger with name '{}' successful persisted", obj.getBurgerName());
    }

    @Override
    public List<Burger> getAll() {
        List<Burger> burgers = entityManager.createNamedQuery(Burger.FIND_ALL_BURGERS).getResultList();
        return burgers;
    }
}
