package com.loibl_michael.burgergrill.business.burger.control.repository;

/**
 *  @author Loibl_Michael
 *  @since 04.01.2019
 *  
 */
public enum RepositoryType {
    BURGER_REPOSITORY,
    INGREDIENT_REPOSITORY,
    ORDER_REPOSITORY,
}
