package com.loibl_michael.burgergrill.business.burger.boundary;

import com.loibl_michael.burgergrill.business.burger.control.OrderService;
import com.loibl_michael.burgergrill.business.burger.control.validation.BurgerNameValidator;
import com.loibl_michael.burgergrill.business.burger.control.validation.ValidOrderPayload;
import com.loibl_michael.burgergrill.business.burger.entity.BurgerOrder;
import com.loibl_michael.burgergrill.business.burger.entity.payload.OrderPayload;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.UUID;

/**
 *  @author Loibl_Michael
 *  @since 23.12.2018
 *  
 */
@Path("orders")
@Stateless
public class OrderResource {

    @Inject
    private OrderService orderService;

    @Inject
    private BurgerNameValidator burgerNameValidator;

    @Inject
    private Logger logger;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createOrder(@ValidOrderPayload OrderPayload orderPayload, final @Context UriInfo uriInfo) {
        logger.info("incoming order-request to {} with payload {}", uriInfo.getBaseUri().toString() + uriInfo.getPath(), orderPayload.toString());
        burgerNameValidator.validate(orderPayload.getBurgerName());
        BurgerOrder burgerOrder = orderService.createOrderFor(orderPayload);
        return Response.created(uriInfo.getAbsolutePathBuilder().path(burgerOrder.getOrderNumber().toString()).build())
                .entity(burgerOrder)
                .build();
    }

    @GET
    @Path("{orderNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrderByOrderNumber(@PathParam("orderNumber") @NotNull UUID orderNumber) {
        BurgerOrder burgerOrder = orderService.getOrderByOrderNumber(orderNumber.toString());
        return Response.ok(burgerOrder).build();
    }
}
