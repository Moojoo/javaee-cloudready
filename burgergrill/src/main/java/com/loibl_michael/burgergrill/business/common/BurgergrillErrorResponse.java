package com.loibl_michael.burgergrill.business.common;

import java.time.LocalDateTime;

/**
 *  @author Loibl_Michael
 *  @since 04.01.2019
 *  
 */
public class BurgergrillErrorResponse {

    private String currentDateAndTime = LocalDateTime.now().toString();
    private String generalMessage = "THIS IS AN ERROR-RESPONSE, AN ERROR OCCORRED IN THE BURGERGRILL-APP, see more details in the message";
    private String message;
    private int httpCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public String getGeneralMessage() {
        return generalMessage;
    }

    public void setGeneralMessage(String generalMessage) {
        this.generalMessage = generalMessage;
    }

    public String getCurrentDateAndTime() {
        return currentDateAndTime;
    }

    public void setCurrentDateAndTime(String currentDateAndTime) {
        this.currentDateAndTime = currentDateAndTime;
    }
}
