package com.loibl_michael.burgergrill.business.burger.boundary;

import com.loibl_michael.burgergrill.business.setting.boundary.SettingService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *  @author Loibl_Michael
 *  @since 05.01.2019
 *  
 */
@Path("offers")
@Stateless
public class OfferResource {

    @Inject
    private SettingService settingService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllOfferedBurgers() {
        return Response.ok(settingService.getBurgerSetting().getReceipes()).build();
    }
}
