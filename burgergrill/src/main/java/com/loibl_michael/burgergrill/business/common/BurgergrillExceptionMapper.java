package com.loibl_michael.burgergrill.business.common;

import org.slf4j.Logger;

import javax.inject.Inject;
import javax.validation.ValidationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *  @author Loibl_Michael
 *  @since 04.01.2019
 *  
 */
@Provider
public class BurgergrillExceptionMapper implements ExceptionMapper<Throwable> {

    @Inject
    private Logger logger;

    @Override
    public Response toResponse(Throwable exception) {
        logger.error("exception occurred in burgergrill-app!", exception);
        BurgergrillErrorResponse errorResponse = createErrorResponse(exception);
        return Response
                .status(errorResponse.getHttpCode())
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(errorResponse).build();
    }

    private BurgergrillErrorResponse createErrorResponse(Throwable exception) {
        BurgergrillErrorResponse errorResponse;
        if (exception instanceof ValidationException) {
            errorResponse = createValidationErrorResponse((ValidationException) exception);
        } else {
            errorResponse = createGeneralErrorResponse(exception);
        }
        return errorResponse;
    }

    private BurgergrillErrorResponse createValidationErrorResponse(ValidationException validationEx) {
        BurgergrillErrorResponse errorResponse = new BurgergrillErrorResponse();
        errorResponse.setMessage("error on validation: " + validationEx.getMessage());
        errorResponse.setHttpCode(Response.Status.BAD_REQUEST.getStatusCode());
        return errorResponse;
    }

    private BurgergrillErrorResponse createGeneralErrorResponse(Throwable ex) {
        BurgergrillErrorResponse errorResponse = new BurgergrillErrorResponse();
        errorResponse.setMessage("error occurred in burgergrill-backend: " + ex.getMessage());
        errorResponse.setHttpCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        return errorResponse;
    }
}
