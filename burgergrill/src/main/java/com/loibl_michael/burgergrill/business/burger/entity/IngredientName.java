package com.loibl_michael.burgergrill.business.burger.entity;

/**
 *  @author Loibl_Michael
 *  @since 31.12.2018
 *  
 */
public enum IngredientName {
    FULL_GRAIN,
    BEEF,
    VEGETABLE_PATTY,
    CHEDDAR,
    TOMATO,
    CUCUMBER,
    SALAT,
    KETCHUP,
    AMERICAN_BURGER_SAUCE,
    VEGETABLE_SOUCE
}
