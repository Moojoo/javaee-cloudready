package com.loibl_michael.burgergrill.business.burger.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.loibl_michael.burgergrill.business.burger.entity.jpa.LocalDateTimeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 *  @author Loibl_Michael
 *  @since 23.12.2018
 *  
 */
@Entity
@Table(name = "burgerorder")
@NamedQueries({
        @NamedQuery(
                name = BurgerOrder.FIND_ALL_ORDERS, query = "SELECT ord FROM BurgerOrder AS ord"
        ),
        @NamedQuery(
                name = BurgerOrder.FIND_ORDER_BY_ORDER_NUMBER, query = "SELECT ord FROM BurgerOrder AS ord WHERE ord.orderNumber = :" + BurgerOrder.ORDER_NUMBER_FOR_QUERY
        )
})
public class BurgerOrder implements Serializable {

    public static final String FIND_ALL_ORDERS = "Ingredient.FIND_ALL_ORDERS";
    public static final String FIND_ORDER_BY_ORDER_NUMBER = "Ingredient.FIND_ORDER_BY_ORDER_NUMBER";
    public static final String ORDER_NUMBER_FOR_QUERY = "orderNumber";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @JsonIgnore
    private Long id;

    @Column(name = "CREATED_AT", updatable = false, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Convert(converter = LocalDateTimeConverter.class)
    @JsonIgnore
    private LocalDateTime createdAt;

    @Column(name = "CUSTOMER_NAME")
    @JsonIgnore
    private String customerName;

    @Column(name = "ORDER_NUMBER")
    private String orderNumber;

    @Column(name = "CALORIE")
    private int calorie;

    @Column(name = "PREPARATION_TIME_IN_SEC")
    private long preparationTimeInSec;

    @Column(name = "TOTAL_PRICE")
    private double totalPrice;

    @Transient
    private String descriptionForCustomer;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_BURGER")
    private Burger burger;

    public BurgerOrder(){
        this.orderNumber = createNewOrderNumber();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getCalorie() {
        return calorie;
    }

    public void setCalorie(int calorie) {
        this.calorie = calorie;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Burger getBurger() {
        return burger;
    }

    public void setBurger(Burger burger) {
        this.burger = burger;
    }

    public long getPreparationTimeInSec() {
        return preparationTimeInSec;
    }

    public void setPreparationTimeInSec(long preparationTimeInSec) {
        this.preparationTimeInSec = preparationTimeInSec;
    }

    public String getDescriptionForCustomer() {
        return descriptionForCustomer;
    }

    public void setDescriptionForCustomer(String descriptionForCustomer) {
        this.descriptionForCustomer = descriptionForCustomer;
    }

    private static String createNewOrderNumber(){
        return UUID.randomUUID().toString();
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }


    @Override
    public String toString() {
        return "BurgerOrder { " +
                "id = " + id +
                ", createdAt = " + createdAt +
                ", customerName = '" + customerName + '\'' +
                ", orderNumber = '" + orderNumber + '\'' +
                ", calorie = " + calorie +
                ", preparationTimeInSec = " + preparationTimeInSec +
                ", totalPrice = " + totalPrice +
                ", descriptionForCustomer = '" + descriptionForCustomer + '\'' +
                ", burger = " + burger +
                '}';
    }
}
