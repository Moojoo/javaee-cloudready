package com.loibl_michael.burgergrill.business.burger.control.validation;

import com.loibl_michael.burgergrill.business.burger.entity.payload.OrderPayload;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *  @author Loibl_Michael
 *  @since 23.12.2018
 *  
 */
public class OrderPayloadValidator implements ConstraintValidator<ValidOrderPayload, OrderPayload> {

    @Override
    public void initialize(ValidOrderPayload constraintAnnotation) {
        //nothing to initialize
    }

    @Override
    public boolean isValid(OrderPayload orderPayload, ConstraintValidatorContext constraintValidatorContext) {
        return isNotNull(orderPayload) &&
                areAllValuesAvailable(orderPayload);
    }

    private boolean isNotNull(OrderPayload orderPayload) {
        return orderPayload != null;
    }

    private boolean areAllValuesAvailable(OrderPayload orderPayload) {
        return orderPayload.getCustomerName() != null && !orderPayload.getCustomerName().isEmpty() &&
                orderPayload.getBurgerName() != null && !orderPayload.getBurgerName().isEmpty();
    }
}
