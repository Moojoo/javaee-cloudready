package com.loibl_michael.burgergrill.business.burger.control;

import org.slf4j.Logger;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *  @author Loibl_Michael
 *  @since 01.01.2019
 *  
 * BurgergrillProgressTimer that calculate the "grill-progress" of all burgers that are still on the grill :-)
 */
@Singleton
public class BurgergrillProgressTimer {

    @Inject
    private Burgergrill burgergrill;

    @Inject
    private Logger logger;

    /**
     * calcGrillProgressOfAllBurgers - calculate the "grill-progress" of all burgers that are still on the grill :-)
     */
    @Lock(LockType.READ)
    @Schedule(second = "*/5", minute = "*", hour = "*", persistent = false)
    public void calcGrillProgressOfAllBurgers() {
        logger.info("TIMER START to calculate grill-progress"); //log by every schedule-run only as showcase
        burgergrill.calcGrillProgress();
    }
}
