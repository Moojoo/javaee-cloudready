package com.loibl_michael.burgergrill.business.burger.entity;

import javax.swing.*;
import java.time.Duration;

/**
 *  @author Loibl_Michael
 *  @since 31.12.2018
 *  
 */
public class BurgerReceiveConfirmation {

    private Burger burger;
    private Duration preparationDurationOfBurger;

    public static BurgerReceiveConfirmation createWith(Burger burger, Duration preparationDurationOfBurger){
        BurgerReceiveConfirmation burgerReceiveConfirmation = new BurgerReceiveConfirmation();
        burgerReceiveConfirmation.setBurger(burger);
        burgerReceiveConfirmation.setPreparationDurationOfBurger(preparationDurationOfBurger);
        return burgerReceiveConfirmation;
    }

    public Burger getBurger() {
        return burger;
    }

    public void setBurger(Burger burger) {
        this.burger = burger;
    }

    public Duration getPreparationDurationOfBurger() {
        return preparationDurationOfBurger;
    }

    public void setPreparationDurationOfBurger(Duration preparationDurationOfBurger) {
        this.preparationDurationOfBurger = preparationDurationOfBurger;
    }
}
