package com.loibl_michael.burgergrill.business.burger.control;

import com.loibl_michael.burgergrill.business.burger.control.repository.RepositoryFactory;
import com.loibl_michael.burgergrill.business.burger.entity.BurgerOrder;
import com.loibl_michael.burgergrill.business.burger.entity.BurgerReceiveConfirmation;
import com.loibl_michael.burgergrill.business.burger.entity.payload.OrderPayload;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.time.LocalDateTime;

/**
 *  @author Loibl_Michael
 *  @since 23.12.2018
 *  
 */
public class OrderService {

    @Inject
    private Burgergrill burgergrill;

    @Inject
    private RepositoryFactory repositoryFactory;

    @Inject
    private Logger logger;

    public BurgerOrder createOrderFor(OrderPayload orderPayload) {
        logger.info("start with creation of new burger order for burger '{}'", orderPayload.getBurgerName());
        BurgerReceiveConfirmation receiveConfirmation = burgergrill.createBurgerFor(orderPayload.getBurgerName());

        BurgerOrder burgerOrder = new BurgerOrder();
        burgerOrder.setCustomerName(orderPayload.getCustomerName());
        burgerOrder.setBurger(receiveConfirmation.getBurger());
        burgerOrder.setPreparationTimeInSec(receiveConfirmation.getPreparationDurationOfBurger().getSeconds());
        burgerOrder.setDescriptionForCustomer(createDescriptionForCustomer(orderPayload.getCustomerName(),
                receiveConfirmation.getPreparationDurationOfBurger().getSeconds()));
        burgerOrder.setCreatedAt(LocalDateTime.now());

        logger.info("new burgerorder successful created: {}", burgerOrder.toString());
        repositoryFactory.getOrderRepository().persist(burgerOrder);

        return burgerOrder;
    }

    public BurgerOrder getOrderByOrderNumber(String orderNumber) {
        BurgerOrder burgerOrder = repositoryFactory.getOrderRepository().getOrderByOrderNumber(orderNumber);
        burgerOrder.setDescriptionForCustomer(createDescriptionForCustomer(burgerOrder.getCustomerName(), burgerOrder.getPreparationTimeInSec()));
        return burgerOrder;
    }

    private String createDescriptionForCustomer(String customerName, long burgerPreparationTime) {
        if (burgerPreparationTime > 0) {
            return createDescrForWaiting(customerName, burgerPreparationTime);
        } else {
            return createDescrForReadyBurger(customerName, burgerPreparationTime);
        }
    }

    private String createDescrForWaiting(String customerName, long burgerPreparationTime) {
        return String.format("Hello %s, thank you for your burger-order. The preparation time of your burger is still <%s seconds>. Please wait.",
                customerName,
                burgerPreparationTime);
    }

    private String createDescrForReadyBurger(String customerName, long burgerPreparationTime) {
        return String.format("Hello %s, thank you for your burger-order. Your burger is finish and you can get it at the counter - Enjoy your meel :)",
                customerName,
                burgerPreparationTime);
    }
}
