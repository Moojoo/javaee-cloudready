package com.loibl_michael.burgergrill.business.burger.control;

import com.loibl_michael.burgergrill.business.burger.control.repository.RepositoryFactory;
import com.loibl_michael.burgergrill.business.burger.control.validation.BurgerNameValidator;
import com.loibl_michael.burgergrill.business.burger.entity.Burger;
import com.loibl_michael.burgergrill.business.burger.entity.BurgerOrder;
import com.loibl_michael.burgergrill.business.burger.entity.BurgerReceiveConfirmation;
import com.loibl_michael.burgergrill.business.burger.entity.Ingredient;
import com.loibl_michael.burgergrill.business.setting.boundary.SettingService;
import com.loibl_michael.burgergrill.business.setting.entity.BurgerSetting;
import com.loibl_michael.burgergrill.business.setting.entity.Receipe;
import com.loibl_michael.burgergrill.business.setting.entity.ReceipeEntry;
import org.slf4j.Logger;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.inject.Inject;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *  @author Loibl_Michael
 *  @since 31.12.2018
 *  
 */
public class Burgergrill {

    @Inject
    private SettingService settingService;

    @Inject
    private RepositoryFactory repositoryFactory;

    @Inject
    private BurgerNameValidator burgerNameValidator;

    @Inject
    private Logger logger;

    private AtomicBoolean calcGrillProgressStillRunning = new AtomicBoolean(false);
    private static final Integer UPPER_BOUND_OF_PREPARATION_TIME = 20;
    private static final Integer LOWER_LIMIT = 15;

    /**
     * Retrieves the name of the burger that should be prepared.
     *
     * @param burgerName - the name of the ordered Burger
     * @return BurgerReceiveConfirmation - informations about the successful retrieved burgerOrder
     */
    public BurgerReceiveConfirmation createBurgerFor(String burgerName) {
        Burger burger = Burger.createBurgerWithoutIngredients(burgerName);
        validateNameOfBurger(burger);

        BurgerSetting burgerSetting = settingService.getBurgerSetting();
        Receipe burgerReceipe = getRelevantBurgerReceipe(burgerName, burgerSetting);
        fillBurgerWithIngredients(burger, burgerReceipe);

        repositoryFactory.getBurgerRepository().persist(burger);

        return BurgerReceiveConfirmation
                .createWith(burger, Duration.ofSeconds(createRandomPreparationTime()));
    }

    @Lock(LockType.READ)
    public void calcGrillProgress() {
        if (!calcGrillProgressStillRunning.compareAndSet(false, true)) {
            return; //return if previous calculation progress is still running.
        }
        calcProgress();
        calcGrillProgressStillRunning.set(false);
    }

    @Lock(LockType.READ)
    private void calcProgress() {
        List<BurgerOrder> allOrders = repositoryFactory.getOrderRepository().getAll();
        for (BurgerOrder order : allOrders) {
            if (order.getPreparationTimeInSec() >= 3) {
                order.setPreparationTimeInSec(order.getPreparationTimeInSec() - 3);
                logRemainingGrillTime(order);
            } else if (order.getPreparationTimeInSec() < 3 && order.getPreparationTimeInSec() != 0) {
                logger.info("NEW BURGER IS FINISH AND READY: burgerOrder {} with burger {}", order.getOrderNumber(), order.getBurger().getBurgerName());
                order.setPreparationTimeInSec(0);
            }
        }
    }

    private void validateNameOfBurger(Burger burger) {
        burgerNameValidator.validate(burger.getBurgerName());
        logger.info("validation of burgername '{}' successful!", burger.getBurgerName());
    }

    private Receipe getRelevantBurgerReceipe(String burgerName, BurgerSetting burgerSetting) {
        return burgerSetting.getReceipes().stream()
                .filter(rec -> rec.getBurgerName().equals(burgerName))
                .findFirst()
                .get();
    }

    private void fillBurgerWithIngredients(Burger burger, Receipe receipe) {
        List<Ingredient> allIngredients = repositoryFactory.getIngredientRepository().getAll();
        for (ReceipeEntry receipeEntry : receipe.getReceipe()) {
            fillBurgerIfIngredientMatch(allIngredients, receipeEntry, burger);
        }
    }

    private void fillBurgerIfIngredientMatch(List<Ingredient> allIngredients, ReceipeEntry receipeEntry, Burger burger) {
        StringBuilder loggingMsgBuilder = new StringBuilder();
        allIngredients.stream()
                .filter(ingr -> ingr.getIngredientName() == receipeEntry.getIngredientName())
                .findFirst()
                .ifPresent(ingr -> {
                    burger.addIngredient(ingr);
                    loggingMsgBuilder.append(appendMessage(ingr));
                    decreaseStockAmount(ingr, receipeEntry);
                    loggingMsgBuilder.append("stock amount after decreasing: " + ingr.getStockAmount() + "\n");
                });
        logger.info(loggingMsgBuilder.toString());
    }

    private void decreaseStockAmount(Ingredient ingredient, ReceipeEntry receipeEntry) {
        ingredient.setStockAmount(ingredient.getStockAmount() - receipeEntry.getAmount());
    }

    private int createRandomPreparationTime() {
        Random randomPrepartionTimeGenerator = new Random();
        int preparationTime = randomPrepartionTimeGenerator.nextInt(UPPER_BOUND_OF_PREPARATION_TIME) + LOWER_LIMIT;
        logger.info("preparation time of burgerorder in sec: {}", preparationTime);
        return preparationTime;
    }

    private String appendMessage(Ingredient ingredient) {
        return "add ingredient to burger :" + ingredient.getIngredientName() + "\n" +
                "current stockAmount: " + ingredient.getStockAmount() + "\n";
    }

    private void logRemainingGrillTime(BurgerOrder order){
        logger.info("calculate grill-progress of burgerOrder {} with burger {}. remaining grill-time : {}",
                order.getOrderNumber(),
                order.getBurger().getBurgerName(),
                order.getPreparationTimeInSec());
    }
}
