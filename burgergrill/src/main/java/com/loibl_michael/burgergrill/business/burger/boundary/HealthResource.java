package com.loibl_michael.burgergrill.business.burger.boundary;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;

import javax.enterprise.context.ApplicationScoped;

/**
 *  @author Loibl_Michael
 *  @since 23.12.2018
 *  
 */
@Health
@ApplicationScoped
public class HealthResource implements HealthCheck {

    @Override
    public HealthCheckResponse call() {
        return HealthCheckResponse.builder()
                .name("burgergrill healthcheck")
                .withData("applicationName", "burgergrill")
                .up()
                .build();
    }
}
