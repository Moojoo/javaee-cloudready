package com.loibl_michael.burgergrill.business.burger.control.repository;

import com.loibl_michael.burgergrill.business.burger.control.repository.burger.IBurgerRepository;
import com.loibl_michael.burgergrill.business.burger.control.repository.ingredient.IIngredientRepository;
import com.loibl_michael.burgergrill.business.burger.control.repository.order.IOrderRepository;

import javax.inject.Inject;

import static com.loibl_michael.burgergrill.business.burger.control.repository.RepositoryType.*;

/**
 *  @author Loibl_Michael
 *  @since 04.01.2019
 *  
 */
public class RepositoryFactory {

    @Inject
    @RepoQualifier(forRepo = BURGER_REPOSITORY)
    private IBurgerRepository burgerRepository;

    @Inject
    @RepoQualifier(forRepo = INGREDIENT_REPOSITORY)
    private IIngredientRepository ingredientRepository;

    @Inject
    @RepoQualifier(forRepo = ORDER_REPOSITORY)
    private IOrderRepository orderRepository;

    public IBurgerRepository getBurgerRepository() {
        return burgerRepository;
    }

    public IIngredientRepository getIngredientRepository() {
        return ingredientRepository;
    }

    public IOrderRepository getOrderRepository() {
        return orderRepository;
    }
}
