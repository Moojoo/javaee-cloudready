package com.loibl_michael.burgergrill.business.setting.control;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loibl_michael.burgergrill.business.setting.entity.BurgerSetting;
import org.eclipse.microprofile.config.spi.Converter;

/**
 *  @author Loibl_Michael
 *  @since 31.12.2018
 *  
 */
public class BurgerConfigConverter implements Converter<BurgerSetting> {

    @Override
    public BurgerSetting convert(String value) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            BurgerSetting burgerSetting = objectMapper.readValue(value, BurgerSetting.class);
            return burgerSetting;
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
