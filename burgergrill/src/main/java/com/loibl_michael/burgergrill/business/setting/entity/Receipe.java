package com.loibl_michael.burgergrill.business.setting.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *  @author Loibl_Michael
 *  @since 31.12.2018
 *  
 */
public class Receipe {

    private String burgerName;
    private List<ReceipeEntry> receipe;

    public Receipe() {
        this.receipe = new ArrayList<>();
    }

    public void addNewReceipe(List<ReceipeEntry> receipe){
        this.receipe = receipe;
    }

    public List<ReceipeEntry> getReceipe() {
        return receipe;
    }

    public void setReceipe(List<ReceipeEntry> receipe) {
        this.receipe = receipe;
    }

    public String getBurgerName() {
        return burgerName;
    }

    public void setBurgerName(String burgerName) {
        this.burgerName = burgerName;
    }
}
