package com.loibl_michael.burgergrill;

import com.loibl_michael.burgergrill.business.burger.boundary.OfferResource;
import com.loibl_michael.burgergrill.business.burger.boundary.OrderResource;
import com.loibl_michael.burgergrill.business.common.BurgergrillExceptionMapper;
import com.loibl_michael.burgergrill.common.CORSFilter;
import org.glassfish.jersey.jackson.JacksonFeature;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 *  @author Loibl_Michael
 *  @since 23.12.2018
 *  
 */
@ApplicationPath("api")
public class JaxRSConfiguration extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<>();
        classes.add(JacksonFeature.class);
        classes.add(OrderResource.class);
        classes.add(OfferResource.class);
        classes.add(BurgergrillExceptionMapper.class);
        classes.add(CORSFilter.class);
        return classes;
    }
}
